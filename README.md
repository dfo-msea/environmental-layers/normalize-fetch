# Normalize Fetch Rasters

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: cole.fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The purpose of this project is to normalize a set of rasters to a common scale (0-1) using the greatest value from the set.


## Summary
This code is used to normalize minimum fetch and sum of fetch rasters that are part of the Species Distribution Modelling (SDM) work. 
The regional layers (NCC, QCS, Salish Sea, NCC, and HG) are normalized coast-wide so that the values across regions are on the same scale
and can be compared. These layers are part of the suite of environmental predictor layers used for modelling. Originally, min fetch and sum fetch were not
normalized. The values were estimates of fetch in metres. However, it may be more useful and standardized to have the rasters normalized
on a 0-1 scale. Only the region with the greatest min fetch or greatest sum of fetch will have a value of 1 in the output raster.


## Status
Completed


## Contents
* requirements.yml: yml file with list of required libraries for code to run.
* normalize.py: code with functions and main function to run.


## Methods
Get the max value from the set of rasters. The following formula is used to normalize on a 0-1 scale: 
(array - array_min)/(array_max - array_min) where array_min is the minimum value in the raster being normalized, 
and array_max is the maximum value from all of the regions. This is done for each region.


## Requirements
#### Clone directory.

`git clone git@gitlab.com:dfo-msea/environmental-layers/normalize-fetch.git`

#### Navigate into directory.

`cd normalize-fetch`

Create a new python 3 environment and install required libraries. Replace 'env' with the a name for your new env. If you do not supply a new name, the 
virtual env name will be `gdal`.

`conda env create --name <env> -f requirements.yml`

#### Activate new virtual env.

`conda activate <env>`

#### Run code.

**Help**

List the required arguments.

`python normalize.py -h`

```
positional arguments:
  input_dir    full path to input directory where raster files are located
  output_dir   full path to output directory where raster files will be written to (must already exist)
```

**Example of running code**

`python normalize.py C:\Temp\inrasters C:\Temp\outrasters`

**Log File Example**

Log file ('normalize-raster.log') will be written to output directory.

```
2021-01-14 09:25:20,135:INFO:normalize(18) - Working directory: C:\Temp\fetch\min
2021-01-14 09:25:20,135:INFO:normalize(19) - Getting tif filepaths...
2021-01-14 09:25:20,135:INFO:normalize(21) - Number of raster layers in C:\Temp\fetch\min: 5
2021-01-14 09:25:20,135:INFO:normalize(38) - Creating raster dataset objects from files...
2021-01-14 09:25:20,151:INFO:normalize(41) - Creating raster band objects from raster dataset objects...
2021-01-14 09:25:20,151:INFO:normalize(44) - Creating list of statistics from raster layers
2021-01-14 09:25:20,151:INFO:normalize(63) - Creating list of minimum values from statistics and adding to dictionary...
2021-01-14 09:25:20,151:INFO:normalize(65) - List of minimum values sorted from in ascending order: [0.0, 0.75603097677231, 0.87055999040604, 1.259578704834, 3.5]
2021-01-14 09:25:20,151:INFO:normalize(69) - Creating list of maximum values from statistics and adding to dictionary...
2021-01-14 09:25:20,151:INFO:normalize(72) - List of maximum values sorted from in ascending order: [5235.81640625, 6289.8857421875, 7047.1508789063, 8819.0, 16464.75]
2021-01-14 09:25:20,151:INFO:normalize(74) - fetch_min_hg.tif min value: 3.5
2021-01-14 09:25:20,151:INFO:normalize(75) - fetch_min_hg.tif max value: 6289.8857421875
2021-01-14 09:25:20,151:INFO:normalize(74) - fetch_min_ncc.tif min value: 0.87055999040604
2021-01-14 09:25:20,151:INFO:normalize(75) - fetch_min_ncc.tif max value: 16464.75
2021-01-14 09:25:20,151:INFO:normalize(74) - fetch_min_qcs.tif min value: 0.75603097677231
2021-01-14 09:25:20,151:INFO:normalize(75) - fetch_min_qcs.tif max value: 5235.81640625
2021-01-14 09:25:20,151:INFO:normalize(74) - fetch_min_salish.tif min value: 0.0
2021-01-14 09:25:20,151:INFO:normalize(75) - fetch_min_salish.tif max value: 8819.0
2021-01-14 09:25:20,151:INFO:normalize(74) - fetch_min_wcvi.tif min value: 1.259578704834
2021-01-14 09:25:20,166:INFO:normalize(75) - fetch_min_wcvi.tif max value: 7047.1508789063
2021-01-14 09:25:20,166:INFO:normalize(76) - {'min': 0.0, 'max': 16464.75}
2021-01-14 09:25:20,166:INFO:normalize(98) - Normalizing fetch_min_hg.tif by dividing all values by 16464.75.
2021-01-14 09:25:25,822:INFO:normalize(98) - Normalizing fetch_min_ncc.tif by dividing all values by 16464.75.
2021-01-14 09:25:46,176:INFO:normalize(98) - Normalizing fetch_min_qcs.tif by dividing all values by 16464.75.
2021-01-14 09:25:49,254:INFO:normalize(98) - Normalizing fetch_min_salish.tif by dividing all values by 16464.75.
2021-01-14 09:26:00,137:INFO:normalize(98) - Normalizing fetch_min_wcvi.tif by dividing all values by 16464.75.
2021-01-14 09:26:11,759:INFO:normalize(136) - Calculating statistics...
2021-01-14 09:26:11,759:INFO:normalize(137) - Creating raster dataset objects from files...
2021-01-14 09:26:11,775:INFO:normalize(140) - Creating raster band objects from raster dataset objects...
2021-01-14 09:26:24,494:INFO:normalize(175) - Processing: complete.
```

## Caveats
* Input directory should have all the rasters for one metric (i.e. all the min fetch rasters). 
* Input rasters will need to have different names (such as min_fetch_region.tif), replacing 'region' with the region 
name. 
* Output file names will be the same as input, but written to the output directory (must already exist). 
* Full paths to directories are expected as input.
* Only works with GeoTIFF raster format.
* Expects that all layers have the same coordinate reference system.


## Uncertainty
*Optional section.* Is there some uncertainty associated with the output? Assumptions that were made?


## Acknowledgements
Jessica Nephin


## References
https://automating-gis-processes.github.io/CSC/notebooks/L5/plotting-raster.html

https://pcjericks.github.io/py-gdalogr-cookbook/raster_layers.html?highlight=statistics#

https://gist.github.com/pratos/e167d4b002f5d888d0726a5b5ddcca57

https://gitlab.com/jnephin/bathy-complexity/-/blob/master/complexity_example.ipynb
