import os
import glob
from osgeo import gdal
import logging
import traceback
import rasterio
import numpy as np


def list_tiffs(input_dir):
    """
    Get a list of TIFF filepaths.
    :param input_dir: input directory.
    :return: list of file names for tiffs.
    """
    try:
        os.chdir(input_dir)
        logging.info("Working directory: %s" % input_dir)
        logging.info("Getting tif filepaths...")
        files = [file for file in glob.glob("*.tif")]
        logging.info("Number of raster layers in %s: %s" % (input_dir, len(files)))
        return files
    except Exception:
        logging.error(traceback.format_exc())


def raster_layers_stats(filepath_list):
    """
    Load raster layers, get bands, and get stats for each layer.
    :param filepath_list: list of filepaths returned from list_tiffs().
    :return: dictionary with raster info: [0] name: name of tif file (with extension .tif)
                                          [1] dataset_object: list of Dataset objects
                                          [2] band_object: list of band layer objects
                                          [3] stats: list of lists, each with raster stat values ([0]: min [1]: max)
    """
    try:
        # list of osgeo.gdal.Dataset objects
        logging.info("Creating raster dataset objects from files...")
        raster_lyrs = [gdal.Open(file) for file in filepath_list]
        # list of osgeo.gdal.Band objects. WARNING: assumes single band rasters only.
        logging.info("Creating raster band objects from raster dataset objects...")
        band_lyrs = [raster.GetRasterBand(1) for raster in raster_lyrs]
        # get raster statistics
        logging.info("Creating list of statistics from raster layers")
        stats = [band.GetStatistics(0, 1) for band in band_lyrs]
        raster_dict = {"name": filepath_list, "dataset_object": raster_lyrs, "band_object": band_lyrs, "stats": stats}
        return raster_dict
    except Exception:
        logging.error(traceback.format_exc())


def get_min_max(raster_dict):
    """
    Create a dictionary with min (of min stat values) and max (of max stat values) values from list of raster stats.
    :param stats_list: list of lists, each with raster stat values (0 - min, 1 - max, 2 - mean, 3 - std) returned from
    raster_stats().
    :return: dictionary with min (of min stat values) and max (of max stat values) values from list of raster stats.
    """
    try:
        # minimum stat value as at index 0.
        # get the minimum of all min value stats.
        stats_list = raster_dict.get("stats")
        logging.info("Creating list of minimum values from statistics and adding to dictionary...")
        min_list = sorted([min_value[0] for min_value in stats_list])  # list of min values for QC.
        logging.info("List of minimum values sorted from in ascending order: %s" % min_list)
        stats_dict = {"min": min([min_value[0] for min_value in stats_list])}
        # max stat value as at index 1.
        # get the maximum of all max value stats.
        logging.info("Creating list of maximum values from statistics and adding to dictionary...")
        max_list = sorted([max_value[1] for max_value in stats_list])  # list of max values for QC.
        stats_dict.update({"max": max([max_value[1] for max_value in stats_list])})
        logging.info("List of maximum values sorted from in ascending order: %s" % max_list)
        for name, stat in zip(raster_dict.get("name"), raster_dict.get("stats")):
            logging.info("{} min value: {}".format(name, stat[0]))
            logging.info("{} max value: {}".format(name, stat[1]))
        logging.info(stats_dict)
        return stats_dict
    except Exception:
        logging.error(traceback.format_exc())


# Function to normalize the grid values
def normalize_grid_values(array, min_max_dict):
    """Normalizes numpy arrays into scale 0.0 - 1.0"""
    array_min, array_max = array.min(), min_max_dict.get("max")
    return (array - array_min)/(array_max - array_min)


def normalize(min_max_dict, raster_dict, outdir):
    try:
        # list of filenames without .tif extention.
        files = []
        # loop dict and normalize raster values.
        for object, band, stat, name in zip(raster_dict.get("dataset_object"),
                                            raster_dict.get("band_object"),
                                            raster_dict.get("stats"),
                                            raster_dict.get("name")):
            logging.info("Normalizing {} by dividing all values by {}.".format(name, min_max_dict.get("max")))
            # new outfile name.
            outfile = os.path.join(outdir, name.split(".")[0] + ".tif")
            files.append(outfile)
            # CREDIT: Jessica Nephin: https://gitlab.com/jnephin/bathy-complexity/-/blob/master/complexity_example.ipynb
            # Open raster file temporarily.
            with rasterio.open(name) as ras:
                # Get profile for output file.
                profile = ras.profile
                # Get as np array
                array = ras.read(1, masked=True)
                # Convert no data to nan.
                # nodata = ras.nodatavals
                # array[array == nodata] = 'nan'
                # normalize values by dividing by greatest value from all rasters.
                array_norm = normalize_grid_values(array, min_max_dict)
                # write to disk.
                with rasterio.Env():
                    # Update profile
                    profile.update(
                        dtype=rasterio.float32,
                        count=1,
                        compress='lzw')
                    # Write raster
                    with rasterio.open(outfile, 'w', **profile) as dataset:
                        dataset.write(array_norm.astype(rasterio.float32), 1)
        # return list of file paths.
        return files
    except Exception:
        logging.error(traceback.format_exc())


def calc_stats(raster_file):
    # Calculate statistics on raster band
    logging.info(f'Calculating statistics for {raster_file}')
    gdal_dataset = gdal.Open(raster_file)
    gdal_band = gdal_dataset.GetRasterBand(1)
    gdal_band.ComputeStatistics(0)
    gdal_band = None
    gdal_dataset = None
    return None


def main():
    # CLI arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir", type=str, help="full path to input directory where raster files are located")
    parser.add_argument("output_dir", type=str,
                        help="full path to existing output directory where raster files will be written to "
                             "(must already exist)")
    args = parser.parse_args()

    # set up logging
    logging.basicConfig(filename=os.path.join(args.output_dir, "normalize-raster.log"),
                        filemode="w",
                        format="%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s",
                        level=logging.INFO)
    console = logging.StreamHandler()
    console.setLevel(logging.ERROR)
    logging.getLogger("").addHandler(console)

    # get list of tif paths
    tif_files = list_tiffs(args.input_dir)
    # make rasters and get bands, generate stats for each
    raster_info = raster_layers_stats(tif_files)
    # create dict of min and max values from list of min values and list of max values
    min_max = get_min_max(raster_info)
    # normalize raster values on 0-1 scale
    norm = normalize(min_max, raster_info, args.output_dir)
    # calculate statistics
    [calc_stats(norm_raster) for norm_raster in norm]
    logging.info("Processing: complete.")


if __name__ == "__main__":
    main()

